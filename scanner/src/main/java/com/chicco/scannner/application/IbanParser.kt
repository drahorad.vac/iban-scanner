package com.chicco.scannner.application

import org.iban4j.CountryCode
import org.iban4j.Iban
import org.iban4j.Iban.valueOf
import org.iban4j.IbanUtil.getCheckDigit
import org.iban4j.IbanUtil.getCountryCode
import java.lang.Character.isDigit

/**
 * Created by Vaclav Drahorad on 08.09.2018.
 */
object IbanParser {

    private const val MAX_IBAN_LENGTH = 34
    private const val MIN_IBAN_CANDIDATE_LENGTH = 4

    fun findIban(words: List<String>) = parseIbans(words, true).firstOrNull()

    fun findAllIbans(words: List<String>) = parseIbans(words)

    private fun parseIbans(words: List<String>, returnFirst: Boolean = false): List<Iban> {
        val ibans = mutableListOf<Iban>()

        words.forEachIndexed { index: Int, word: String ->
            if (word.isIbanCandidate()) {
                tryFindIban(words, index)?.run {
                    if (returnFirst) return listOf(this) else ibans.add(this)
                }
            }
        }
        return ibans
    }

    private fun tryFindIban(words: List<String>, start: Int): Iban? {
        var ibanCandidate = words[start]
        ibanCandidate.toIbanOrNull()?.run { return this }

        words.subList(start + 1, words.size).forEach {
            ibanCandidate += it
            ibanCandidate.toIbanOrNull()?.run { return this }
            if (ibanCandidate.length > MAX_IBAN_LENGTH) return null
        }

        return null
    }

    private fun String.toIbanOrNull(): Iban? {
        return try {
            valueOf(this)
        } catch (ignored: Exception) {
            null
        }
    }

    private fun String.isIbanCandidate(): Boolean {
        if (length < MIN_IBAN_CANDIDATE_LENGTH) return false
        return getCountryCode(this).run {
            if (isEmpty()) return false
            else this@isIbanCandidate.hasPresentDigits() && hasValidCountryCode()
        }
    }

    private fun String.hasValidCountryCode() = CountryCode.getByCode(this.toUpperCase()) != null

    private fun String.hasPresentDigits(): Boolean {
        return length >= MIN_IBAN_CANDIDATE_LENGTH && getCheckDigit(this).run {
            isDigit(this[0]) && isDigit(this[1])
        }
    }
}
