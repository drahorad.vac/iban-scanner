package com.chicco.scannner.application

import com.tngtech.java.junit.dataprovider.DataProvider
import com.tngtech.java.junit.dataprovider.DataProviderRunner
import com.tngtech.java.junit.dataprovider.DataProviders.`$$`
import com.tngtech.java.junit.dataprovider.DataProviders.`$`
import com.tngtech.java.junit.dataprovider.UseDataProvider
import junit.framework.Assert.*
import org.iban4j.Iban
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Vaclav Drahorad on 08.09.2018.
 */
@RunWith(DataProviderRunner::class)
class IbanParserTest {

    private companion object {
        const val ANY_TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras leo ante, tincidunt nec fringilla eu, euismod eget turpis. Donec purus tellus, mattis ut lacus vel, tincidunt mattis quam. Nam nec suscipit nisi. Aliquam erat volutpat. Nunc finibus vehicula tortor a pretium. Nam maximus augue sit amet mi tristique cursus. Pellentesque non ornare nibh. Cras sed ultricies mi. Ut mollis, purus nec consequat ultrices, nulla diam suscipit lectus, et egestas mi tortor non orci."
        val ANY_WORDS_LIST = ANY_TEXT.split(" ")
        const val ANY_IBAN_WITH_SPACES = "DK50 0040 0440 1162 43"
        const val ANY_IBAN_WITHOUT_SPACES = "AZ21NABZ00000000137010001944"
        const val ANY_IBAN_1 = "BR18 0036 0305 0000 1000 9795 493C 1"
        const val ANY_IBAN_2 = "FR14 2004 1010 0505 0001 3M02 606"
        const val ANY_IBAN_3 = "JO94 CBJO 0010 0000 0000 0131 0003 02"

        @DataProvider
        @JvmStatic
        fun dataProviderWordsWithIbans(): Array<out Array<Any>> {
            return `$$`(
                    // @formatter:off
                    `$`(ANY_WORDS_LIST.toMutableList().apply {
                        addAll(ANY_IBAN_WITHOUT_SPACES.toWords())
                        addAll(ANY_WORDS_LIST)
                    }, listOf(ANY_IBAN_WITHOUT_SPACES)),
                    `$`(ANY_WORDS_LIST.toMutableList().apply {
                        addAll(ANY_IBAN_WITH_SPACES.toWords())
                        addAll(ANY_WORDS_LIST)
                    }, listOf(ANY_IBAN_WITH_SPACES)),
                    `$`(ANY_WORDS_LIST.toMutableList().apply {
                        addAll(ANY_IBAN_WITH_SPACES.toWords())
                        addAll(ANY_WORDS_LIST)
                        addAll(ANY_IBAN_WITHOUT_SPACES.toWords())
                    }, listOf(ANY_IBAN_WITH_SPACES, ANY_IBAN_WITHOUT_SPACES)),
                    `$`(ANY_IBAN_1.toWords().toMutableList().apply {
                        addAll(ANY_IBAN_WITHOUT_SPACES.toWords())
                        addAll(ANY_IBAN_2.toWords())
                        addAll(ANY_IBAN_3.toWords())
                    }, listOf(ANY_IBAN_1, ANY_IBAN_WITHOUT_SPACES, ANY_IBAN_2, ANY_IBAN_3)),
                    `$`(ANY_IBAN_1.toWords().toMutableList().apply {
                        addAll(ANY_IBAN_WITHOUT_SPACES.toWords())
                        addAll(ANY_IBAN_2.toWords())
                        addAll(ANY_IBAN_3.toWords())
                        addAll(ANY_WORDS_LIST)
                    }, listOf(ANY_IBAN_1, ANY_IBAN_WITHOUT_SPACES, ANY_IBAN_2, ANY_IBAN_3))
            )
        }

        private fun String.toWords() = split(" ")
    }

    @Test
    @UseDataProvider("dataProviderWordsWithIbans")
    fun shouldFindSingleIban(words: List<String>, expectedIbans: List<String>) {
        val result = IbanParser.findIban(words.toList())

        assertEquals(getIbanFormattedString(expectedIbans.first()), result!!.toFormattedString())
    }

    private fun getIbanFormattedString(ibanString: String) =
            Iban.valueOf(ibanString.removeSpaces()).toFormattedString()

    @Test
    @UseDataProvider("dataProviderWordsWithIbans")
    fun shouldFindAllIbans(words: List<String>, expectedIbans: List<String>) {
        val result = IbanParser.findAllIbans(words.toList())

        assertEquals(expectedIbans.size, result.size)
        result.forEachIndexed { index: Int, resultIban: Iban ->
            assertEquals(getIbanFormattedString(expectedIbans[index]), resultIban.toFormattedString())
        }
    }

    @Test
    fun shouldReturnNullOnFindIbanIfNoIbanPresent() {
        val result = IbanParser.findIban(ANY_WORDS_LIST)

        assertNull(result)
    }

    @Test
    fun shouldReturnNullOnFindIbanIfNoWordsPresent() {
        val result = IbanParser.findIban(listOf())

        assertNull(result)
    }

    @Test
    fun shouldReturnEmptyListOnFindAllIbansIfNoIbanPresent() {
        val result = IbanParser.findAllIbans(ANY_WORDS_LIST)

        assertTrue(result.isEmpty())
    }

    @Test
    fun shouldReturnEmptyListOnFindAllIbansIfNoWordsPresent() {
        val result = IbanParser.findAllIbans(listOf())

        assertTrue(result.isEmpty())
    }

    private fun String.removeSpaces() = replace(" ", "")
}